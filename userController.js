var Users = {
    user1: 'Ravi',
    user2: 'Kalymba',
    user3: 'Jitsu'
};


function index(res) {
    console.log("'index' action was called upon request.");
    res.writeHead(200, { "Content-Type": "text/plain" });

    var name, user;

    for (user in Users) {
        name = Users[user];
        console.log(name);
        res.write(name + '\n');
    }
    res.end();
}

function show(res) {
    console.log("'show' action was called upon request.");
    res.writeHead(200, { "Content-Type": "text/plain" });

    var int = Math.floor(Math.random()  * 3) + 1;

    res.write(Users['user'+int]);
    res.end();
}

exports.index = index;
exports.show = show;


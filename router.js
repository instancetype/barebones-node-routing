function route(pathname, routes, res) {
    console.log('Routing request for ' + pathname);

    if (typeof routes[pathname] === "function") {
        routes[pathname](res);
    } else {
        console.log('404: There is no action associated with ' + pathname);
    }
}

exports.route = route;
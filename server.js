var http = require('http');
var url = require('url');

var port = process.env.PORT || 3000;

function pathTracker(route, routes) {
    function onRequest(req, res) {

        var pathname = url.parse(req.url).pathname;
        console.log('Received request for: ' + pathname);


        route(pathname, routes, res);
    }
    http.createServer(onRequest).listen(port);
    console.log('Server running on port ' + port);
}

exports.pathTracker = pathTracker;
var server = require('./server'),
    router = require('./router'),
    UserController = require('./userController');

var routes = {};
routes['/'] = UserController.index;
routes['/index'] = UserController.index;
routes['/show'] = UserController.show;

server.pathTracker(router.route, routes);